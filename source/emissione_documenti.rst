Emissione Documenti
===================

Vediamo ora come emettere un nuovo documento, in questo esempio creeremo una nuova **fattura di vendita**. 

1. Nella parte sinistra selezioniamo uno o più articoli da aggiungere al corpo del documento;
2. Procediamo premendo il tasto **Documento** e selezioniamo **Fattura**
3. Premiamo il tasto **Conferma** per validare il documento e procedere con la stampa.



Stampa del documento
--------------------

Questa e' una sottosezione

Invio del documento tramite email
---------------------------------

Questa e' un'altra sottosezione




