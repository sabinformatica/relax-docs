Gestione categorie ed articoli
=================
In questo capitolo andreamo a definire l'anagrafia degli articoli.

L’uso della sezione articoli permette di organizzare al meglio i prodotti o i servizi che acquistiamo e/o vendiamo. La sezione articoli velocizza notevolmente l’inserimento delle singole righe nei documenti (fatture, preventivi, ddt, ...) e la gestione dei listini prezzi. In **RELAX**  inoltre, viene data la possibilità di tenere sempre aggiornato il magazzino, di conoscere la disponibilità, i tempi di riordino, la scorta minima, etc.


Configurazione, modifica e cancellazione articoli 
-------------------------------------------------
Avviando il gestionale **RELAX** ci ritroveremo l'interfaccia sottostante:

.. image:: _static/Interfaccia.png

Nella sezione *Gestione* possiamo iniziare la configurazione dei nostri articoli,il primo passo da seguire è quello della creazione di una o diverse categorie alla quale associare i nostri articoli:

Cliccando sulla finestra *Categorie* passiamo allora alla creazione o alla  modificazione di una categoria di articoli, agendo sul pulsante *Nuovo* in basso a sinistra,il gestionale automaticamente aprirà la seguente finestra:

.. image:: _static/categorie.png

la configurazione della categoria passa per l'inserimanto di quattro parametri fonadamentali:

1. *Codice*: il gestionale assegnerà in modo standard un codice numerico alla categoria che potrà essere modificato manualmente dall'utente;

2. *Descrizione*: una breve definizione della categoria che si sta configurando, es. 'Primi Piatti';

3. *Aliquota iva*: ossia l'utente dovra inserire manualmente la tipologia dell'aliquota alla quale appartiene alla suddetta categoria;

4. *Ordinamento*: anche in questo caso **RELAX** associa in modo standard un numero (1000) alla categoria,questo numero(modificabile manualemnte) è il parametro da utilizzare per dare un ordine alle categorie es. 1-2-3-1000...