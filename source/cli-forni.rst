Compilazione struttura anagrafica
=================================


Clienti/Fornitori
-----------------

In questo capitolo andremo a definire la configurazione dei clienti fornitori e operatori in **Relax**.

La configurazione del menu clienti è fondamentale per sfruttare appieno le potenzialità del gestionale, chiamati anche anagrafiche sono le voci alle quali viene associato ogni documento (fattura, preventivo, ddt, etc.). Ogni nominativo dell'anagrafica contiene dati essenziali come la ragione sociale, l'indirizzo, la partita iva o il codice fiscale. Oltre a questi dati è possibile specificare ulteriori campi come il numero di telefono, l’indirizzo di e-mail, le condizioni commerciali, le annotazioni, ecc.. Un soggetto dell'anagrafica può essere cliente, fornitore o entrambe le tipologie.
 
Accedendo alla specifica sezione Gestione su **Relax** il gestionale aprirà l'interfaccia sottostante


.. image:: _static/Interfaccia.png 




Ora esamineremo in dettaglio proprio questa interfaccia di configurazione.

 
Per iniziare, fare clic sul relativo pulsante(clienti o fornitori) per creare modificare o cancellare una di queste figure.
La schermata che si aprirà su **Relax** è un elenco con una parte dei clienti già configurati sul gestionale, in alto a questo menu troviamo un barra di ricerca attraverso la quale è possibile trovare un cliente oppure cliccando in basso a sinistra sul pulsante **Nuovo** è possibile crearne uno.


.. image:: _static/ricerca.png




Scelto o creato il cliente sul quale si vogliono apportare modifiche, **Relax** aprirà l'interfaccia attraverso la quale possiamo codificare l'anagrafia del cliente attraverso i seguenti campi:

- DENOMINAZIONE ossia il nome con il quale inseriremo il cliente dentro il gestionale;
- INDIRIZZO;
- CAP;
- CITTA;
- PROVINCIA;
- CODICE FISCALE;
- PARTITA IVA;
- CELLULARE/TELEFONO; 
- INDIRIZZO EMAIL del clienti, questi campi risultano essere obbligatori per la compilazione anagrafica.

Oltre ai campi citati precedentemente per compilazione del cliente troviamo dei campi secondari quali (WEBSITE,PEC ecc), inoltre sulla parte inferiore della finestra di configuarazione abbiamo la possibilità di avere la situazione finaziaria corrente di ciascun cliente attraverso i campi: SALDO INIZIALE, DARE AVERE e SALDO FINALE.


.. image:: _static/clienti.png 



Compilata la prima parte della struttura anagrafica del cliente,ci possiamo spostare su altre 3 finestre attraverso le quali **Relax** ci da la possibilita di:
- finestra FIDELITY associare una Carta di raccolta punti attraverso la quale si possa usufruire di sconti e promozioni;
- finestra CONDIZIONI di assegnare al cliente/fornitore una categoria di sconto;
- finestra ALTRO possiamo inserire al cliente un commento o nota descrittiva. 

.. image:: _static/finestre.png




Alla fine di questi passaggi cliccando sull'icona SALVA in basso a sinistra dell'interfaccia, tutti i le nostre modifiche verranno registrate su **Relax**.


**NB** la procedura della compilazione anagrafica del cliente vale anche per i fornitori, interfaccia campi e finestre sono identici.
