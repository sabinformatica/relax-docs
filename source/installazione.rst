Installazione
=============

Relax supporta i seguenti sistemi operativi:

- Windows:  2k, XP, Vista, 7, 8, 8.1 and 10, 32 e 64bit.
- FreeBSD/Linux:  gtk 2.8, qt4.5, qt5.6, 32 e 64bit.
- Mac OS X 10.5 o superiore; 

Per installare Relax scaricate la versione più recente dal sito ufficiale https://relax.solutions/downloads
