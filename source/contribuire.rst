Contribuire
===========

La documentazione di Relax è redatta secondo il formato reStructuredText [1]_. Usiamo un tool in Python chiamato Sphinx [2]_ per 
generare a partire dai file in formato reStructuredText diversi formati in uscita: HTML, PDF, ePub, ecc. 

Il sorgente della documentazione, che si compone di un insieme di file .rst che contengono la documentazione vera e propria e da alcuni 
file .py con all'interno delle configurazioni per la generazione, è mantenuto sotto controllo di versione usando git [3]_. Il repository 
git principale è memorizzato nei server di Bitbucket [4]_.


Pull
----

Il primo passo da fare prima di iniziare a lavorare è quello di ottenere l'ultima versione aggiornata dei sorgenti della documentazione
dal repository centrale. Per farlo eseguiamo questi comandi:

1. Apriamo un terminale e spostiamoci nella cartella dove si trovano i sorgenti. In questo esempio la cartella relax-docs è contenuta
   nella home directory identificata dal simbolo ~ ::

      cd ~/relax-docs 

2. Recuperiamo la versione aggiornata usando git::

      git pull



Leggiamo bene l'output di questo comando. Se abbiamo modificato dei file locali ed altre persone hanno modificato gli stessi 
file nel repository centrale ci potrebbero essere dei conflitti che vanno risolti prima di continuare. 
Se abbiamo fatto una modifica locale ad un file e vogliamo annullarla prendendo invece l'ultima versione presente nel repository 
possiamo eseguire il comando::

   git checkout <nomefile>

.. important:: Eseguendo il checkout su un file perderemo tutte le modifiche fatte localmente, il file verrà sostituito con la copia
               presente nel repository centrale. 



Dopo aver risolto eventuali conflitti, possiamo eseguire nuovamente il comando git pull. Se l'operazione va a buon fine avremo nella
nostra cartella locale relax-docs l'ultima versione aggiornata dei sorgenti della documentazione. 

Possiamo aprire ora il nostro editor di testo preferito (Per esempio Sublime [5]_) ed iniziare ad apportare modifiche. 
Nella successiva sezione vedremo i passi da fare per visualizzare il risultato di queste modifiche locali.

Build
-----

Di seguito i passaggi per generare la documentazione in formato HTML a partire dal codice sorgente.  

1. Apriamo un terminale e spostiamoci nella cartella dove si trovano i sorgenti. In questo esempio la cartella relax-docs è contenuta
   nella home directory identificata dal simbolo ~ ::

      cd ~/relax-docs 

2. Attiviamo virtualenv::

      . venv/bin/activate

3. Generiamo la documentazione::

      make html

4. Controlliamo l'output del comando per verificare che non ci siano errori (evidenziati in rosso). Eventualmente correggiamo gli 
   errori segnalati e ripetiamo il comando 3. Infine se tutto viene generato senza errori possiamo aprire la documentazione generata 
   dalla cartella build/html::

      open build/html/index.html


Push
----

Dopo aver apportato delle modifiche alla copia locale della documentazione si possono fare uno o piu' commit locali 
con i seguenti comandi:

1. Apriamo un terminale e spostiamoci nella cartella dove si trovano i sorgenti::

      cd ~/relax-docs
   
2. Controlliamo quali file abbiamo modificato::

      git status 

3. Aggiungiamo nell'area di stage i file che vogliamo committare::

      git add <nomefile>
 
   se vogliamo aggiungere tutti i file modificati all'area di stage possiamo passare il punto come secondo parametro del comando git::


      git add .

4. A questo punto possiamo fare il commit dei file nell'area stage, ricordando di indicare un messaggio chiaro del lavoro fatto
   tra i doppi apici::

      git commit -m "Ho esteso la documentazione articoli."

5. Possiamo fare altre modifiche e ripetere i passi 2-4 quante volte vogliamo, tutti i nostri commit saranno memorizzati 
   **solo** nella nostra copia locale e avremo la possibilità di tornare indietro annullando dei commit fatti in caso di errore. 
   Quando siamo pronti per inviare tutti i commit fatti localmente al repository centrale possiamo eseguire il comando::

      git push


Se il comando git push va a buon fine tutto il lavoro fatto sarà trasferito su bitbucket e la nuova versione della 
documentazione HTML verrà automaticamente generata e pubblicata nel sito ufficiale di Relax.     

.. [1] Guida al formato reStructuredText http://docutils.sourceforge.net/docs/user/rst/quickref.html 
.. [2] Pagina ufficiale del progetto Sphinx http://www.sphinx-doc.org/en/master/
.. [3] Pagina ufficiale del sistema di controllo di versione distribuito Git https://git-scm.com/
.. [4] Il repository ufficiale della documentazione di Relax si trova qui: https://bitbucket.org/sabinformatica/relax-docs/
.. [5] Un ottimo editor che consigliamo è Sublime https://www.sublimetext.com/ 
